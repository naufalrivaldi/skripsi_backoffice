<!doctype html>
<th lang="en">

<head>
  <!-- header -->
  <?php $this->load->view('admin/layout/header') ?>
</head>

<body>
  <div class="dashboard-main-wrapper">
    <!-- navbar -->
    <?php $this->load->view('admin/layout/navbar') ?>
    
    <!-- sidebar -->
    <?php $this->load->view('admin/layout/sidebar') ?>
      
    <!-- wrapper  -->
    <div class="dashboard-wrapper">
      <div class="container-fluid  dashboard-content">
        <!-- breadcrumb -->
        <?php $this->load->view('admin/layout/breadcrumb') ?>
        <!-- alert -->
        <?php $this->load->view('admin/layout/alert') ?>

        <!-- content -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">

              <div class="card-header">
                <a href="<?= site_url('tenant') ?>" class="btn btn-primary">
                  <i class="fas fa-arrow-alt-circle-left"></i> Kembali
                </a>
              </div>

              <div class="card-body">
                <div class="row justify-content-md-center">
                  <div class="col-md-6">
                    <form action="<?= (empty($tenant->id))? site_url('tenant/store') : site_url('tenant/update') ?>" method="POST">
                      <?php if(!empty($tenant->id)): ?>
                        <input type="hidden" name="id" value="<?= $tenant->id ?>">
                      <?php endif ?>
                      <div class="form-group">
                        <label for="kode">Kode</label>
                        <input type="text" name="kode" class="form-control" id="kode" value="<?= $tenant->kode ?>">

                        <small class="text-danger"><?= form_error('kode') ?></small>
                      </div>

                      <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" value="<?= $tenant->nama ?>">

                        <small class="text-danger"><?= form_error('nama') ?></small>
                      </div>

                      <div class="form-group">
                        <label for="pic">PIC</label>
                        <input type="text" name="pic" class="form-control" id="pic" value="<?= $tenant->pic ?>">

                        <small class="text-danger"><?= form_error('pic') ?></small>
                      </div>

                      <div class="form-group">
                        <label for="telp">Telepon</label>
                        <input type="text" name="telp" class="form-control" id="telp" value="<?= $tenant->telp ?>">

                        <small class="text-danger"><?= form_error('telp') ?></small>
                      </div>
                      
                      <?php if(!empty($tenant->id)): ?>
                      <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                          <option value="">Pilih</option>
                          <option value="0" <?= ($tenant->status == '0')?'selected':'' ?>>Nonaktif</option>
                          <option value="1" <?= ($tenant->status == '1')?'selected':'' ?>>Aktif</option>
                        </select>

                        <small class="text-danger"><?= form_error('status') ?></small>
                      </div>
                      <?php endif ?>

                      <div class="form-group">
                        <label for="lokasiId">Lokasi</label>
                        <select name="lokasiId" id="lokasiId" class="form-control">
                          <?php if(!empty($tenant->id)): ?>
                            <option value="<?= $tenant->lokasiId ?>" selected><?= $tenant->kodeLokasi.' - '.$tenant->namaLokasi ?></option>
                          <?php endif ?>
                        </select>

                        <small class="text-danger"><?= form_error('lokasiId') ?></small>
                      </div>

                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                      <button type="reset" class="btn btn-warning"><i class="fas fa-sync"></i> Batal</button>
                    </form>
                  </div>
                </div>
              </div>

              <div class="card-footer">
                <p>*Password akan terset default '12345'</p>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <?php $this->load->view('admin/layout/footer') ?>
    </div>
  </div>
  
  <!-- Optional JavaScript -->
  <?php $this->load->view('admin/layout/javascript') ?>

  <script>
    $('#lokasiId').select2({
      placeholder: 'Cari lokasi',
      theme: "bootstrap",
      ajax: {
        url: "<?= site_url('tenant/loadLokasi') ?>",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: $.map(data, function(item){
              return {
                text: item.kode+' - '+item.nama,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
  </script>
</body>
 
</html>