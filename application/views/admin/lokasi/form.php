<!doctype html>
<th lang="en">

<head>
  <!-- header -->
  <?php $this->load->view('admin/layout/header') ?>
</head>

<body>
  <div class="dashboard-main-wrapper">
    <!-- navbar -->
    <?php $this->load->view('admin/layout/navbar') ?>
    
    <!-- sidebar -->
    <?php $this->load->view('admin/layout/sidebar') ?>
      
    <!-- wrapper  -->
    <div class="dashboard-wrapper">
      <div class="container-fluid  dashboard-content">
        <!-- breadcrumb -->
        <?php $this->load->view('admin/layout/breadcrumb') ?>
        <!-- alert -->
        <?php $this->load->view('admin/layout/alert') ?>

        <!-- content -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">

              <div class="card-header">
                <a href="<?= site_url('lokasi') ?>" class="btn btn-primary">
                  <i class="fas fa-arrow-alt-circle-left"></i> Kembali
                </a>
              </div>

              <div class="card-body">
                <div class="row justify-content-md-center">
                  <div class="col-md-6">
                    <form action="<?= (empty($lokasi->id))? site_url('lokasi/store') : site_url('lokasi/update') ?>" method="POST">
                      <?php if(!empty($lokasi->id)): ?>
                        <input type="hidden" name="id" value="<?= $lokasi->id ?>">
                      <?php endif ?>
                      <div class="form-group">
                        <label for="kode">Kode</label>
                        <input type="text" name="kode" class="form-control" id="kode" value="<?= $lokasi->kode ?>">

                        <small class="text-danger"><?= form_error('kode') ?></small>
                      </div>

                      <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" value="<?= $lokasi->nama ?>">

                        <small class="text-danger"><?= form_error('nama') ?></small>
                      </div>

                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                      <button type="reset" class="btn btn-warning"><i class="fas fa-sync"></i> Batal</button>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <?php $this->load->view('admin/layout/footer') ?>
    </div>
  </div>
  
  <!-- Optional JavaScript -->
  <?php $this->load->view('admin/layout/javascript') ?>
</body>
 
</html>