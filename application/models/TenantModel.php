<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TenantModel extends CI_Model {
    
  public $kode;
  public $password;
  public $nama;
  public $pic;
  public $telp;
  public $status;
  public $lokasiId;

  public function rules(){
    $id = '';
    if(empty($this->input->post('id'))){
      $id = '|is_unique[tenant.kode]';
    }

    return [
      [
        'field' => 'kode',
        'label' => 'kode',
        'rules' => 'required'.$id
      ],
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ],
      [
        'field' => 'pic',
        'label' => 'pic',
        'rules' => 'required'
      ],
      [
        'field' => 'telp',
        'label' => 'telp',
        'rules' => 'required'
      ],
      [
        'field' => 'lokasiId',
        'label' => 'lokasiId',
        'rules' => 'required'
      ]
    ];
  }

  // data
  public function showAll(){
    $status = '';
    if($_GET){
      $status = $_GET['status'];
    }

    return $this->db->like('status', $status)->order_by('kode', 'asc')->get('tenant')->result();
  }

  public function getData($id){
    return $this->db
                  ->select('t.id, t.kode, t.nama, t.pic, t.telp, t.status, t.lokasiId, l.kode as kodeLokasi, l.nama as namaLokasi')
                  ->from('tenant t')
                  ->join('lokasi l', 'l.id = t.lokasiId')
                  ->where('t.id', $id)
                  ->get()->row();
  }

  public function getDataActive($id){
    return $this->db
                  ->select('t.id, t.kode, t.nama, t.pic, t.telp, t.status, t.lokasiId, l.kode as kodeLokasi, l.nama as namaLokasi')
                  ->from('tenant t')
                  ->join('lokasi l', 'l.id = t.lokasiId')
                  ->where('t.id', $id)
                  ->get()->row();
  }

  public function save(){
    $post = $this->input->post();
    $this->kode = $post['kode'];
    $this->password = sha1('12345');
    $this->nama = $post['nama'];
    $this->pic = $post['pic'];
    $this->telp = $post['telp'];
    $this->status = '1';
    $this->lokasiId = $post['lokasiId'];

    return $this->db->insert('tenant', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'kode' => $post['kode'],
      'nama' => $post['nama'],
      'pic' => $post['pic'],
      'telp' => $post['telp'],
      'status' => $post['status'],
      'lokasiId' => $post['lokasiId']
    );

    return $this->db->where('id', $post['id'])->update('tenant', $data);
  }

  public function delete(){
    $id = $this->input->post('id');
    
    return $this->db->where('id', $id)->delete('tenant');
  }

  public function resetPassword(){
    $post = $this->input->post();
    $data = array(
      'kode' => sha1('12345')
    );

    return $this->db->where('id', $post['id'])->update('tenant', $data);
  }

  public function count(){
    return $this->db->where('status', '1')->get('tenant')->num_rows();
  }
    
}
