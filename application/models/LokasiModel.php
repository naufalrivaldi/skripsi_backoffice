<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LokasiModel extends CI_Model {
    
  public $kode;
  public $nama;

  public function rules(){
    $id = '|is_unique[lokasi.kode]';
    if(!empty($this->input->post('id'))){
      $id = '';
    }

    return [
      [
        'field' => 'kode',
        'label' => 'kode',
        'rules' => 'required'.$id
      ],
      [
        'field' => 'nama',
        'label' => 'nama',
        'rules' => 'required'
      ]
    ];
  }

  // data
  public function showAll(){
    return $this->db->order_by('kode', 'asc')->get('lokasi')->result();
  }

  public function getData($id){
    return $this->db->where('id', $id)->get('lokasi')->row();
  }

  public function save(){
    $post = $this->input->post();
    $this->kode = $post['kode'];
    $this->nama = $post['nama'];

    return $this->db->insert('lokasi', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'kode' => $post['kode'],
      'nama' => $post['nama']
    );

    return $this->db->where('id', $post['id'])->update('lokasi', $data);
  }

  public function delete(){
    $id = $this->input->post('id');
    
    return $this->db->where('id', $id)->delete('lokasi');
  }
    
}
