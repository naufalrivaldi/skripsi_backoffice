<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceModel extends CI_Model {
    
  public $nomer;
  public $tgl;
  public $tglJatuhTempo;
  public $keterangan;
  public $total;
  public $ppn;
  public $grandTotal;
  public $note;
  public $userId;
  public $tenantId;

  public function rules(){
    $id = '';
    if(empty($this->input->post('id'))){
      $id = '|is_unique[invoice.nomer]';
    }

    return [
      [
        'field' => 'nomer',
        'label' => 'nomer',
        'rules' => 'required'.$id
      ],
      [
        'field' => 'tgl',
        'label' => 'tgl',
        'rules' => 'required'
      ],
      [
        'field' => 'jatuhTempo',
        'label' => 'jatuhTempo',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'keterangan',
        'rules' => 'required'
      ],
      [
        'field' => 'total',
        'label' => 'total',
        'rules' => 'required'
      ],
      [
        'field' => 'ppn',
        'label' => 'ppn',
        'rules' => 'required'
      ],
      [
        'field' => 'grandTotal',
        'label' => 'grandTotal',
        'rules' => 'required'
      ],
      [
        'field' => 'note',
        'label' => 'note',
        'rules' => 'required'
      ],
      [
        'field' => 'tenantId',
        'label' => 'tenantId',
        'rules' => 'required'
      ]
    ];
  }

  // data
  public function showAll(){
    $tenantId = '';
    $tglA = '';
    $tglB = '';
    if($_GET){
      if(!empty($_GET['tenantId'])){
        $tenantId = $_GET['tenantId'];
      }
      $tglA = $_GET['tglA'];
      $tglB = $_GET['tglB'];
      
      if(empty($tglB)){
        return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->order_by('i.tgl', 'desc')
                ->like('i.tenantId', $tenantId)
                ->like('i.tgl', $tglA)
                ->get()->result();
      }else{
        return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->order_by('i.tgl', 'desc')
                ->like('i.tenantId', $tenantId)
                ->where('i.tgl >=', $tglA)
                ->where('i.tgl <=', $tglB)
                ->get()->result();
      }
    }

    return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->order_by('i.tgl', 'desc')
                ->like('i.tenantId', $tenantId)
                ->get()->result();
  }

  public function showReport(){
    $tenantId = '';
    $tglA = '';
    $tglB = '';
    if(!empty($_GET['tenantId'])){
      $tenantId = $_GET['tenantId'];
    }
    $tglA = $_GET['tglA'];
    $tglB = $_GET['tglB'];
    
    if(empty($tglB)){
      return $this->db
              ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
              ->from('invoice i')
              ->join('tenant t', 't.id = i.tenantId')
              ->order_by('i.tgl', 'asc')
              ->like('i.tenantId', $tenantId)
              ->like('i.tgl', $tglA)
              ->get()->result();
    }else{
      return $this->db
              ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
              ->from('invoice i')
              ->join('tenant t', 't.id = i.tenantId')
              ->order_by('i.tgl', 'asc')
              ->like('i.tenantId', $tenantId)
              ->where('i.tgl >=', $tglA)
              ->where('i.tgl <=', $tglB)
              ->get()->result();
    }
  }


  public function getData($id){
    return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.keterangan, i.total, i.ppn, i.grandTotal, i.note, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant, t.pic, t.telp')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->where('i.id', $id)
                ->get()->row();
  }

  public function showTenantInvoice($id){
    return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->order_by('i.tgl', 'desc')
                ->where('i.tenantId', $id)
                ->get()->result();
  }

  public function cekPembayaran($id){
    return $this->db
                ->select('i.id, i.nomer, i.tgl, i.tglJatuhTempo, i.grandTotal, i.userId, i.tenantId, t.kode as kodeTenant, t.nama as namaTenant')
                ->from('invoice i')
                ->join('tenant t', 't.id = i.tenantId')
                ->where('i.tenantId', $id)
                ->like('i.tgl', date('Y-m'))
                ->get()->row();
  }

  public function save(){
    $post = $this->input->post();
    $jatuhTempo = date('Y-m-d', strtotime($post['tgl'] . '+'.$post['jatuhTempo'].' day'));

    $this->nomer = $post['nomer'];
    $this->tgl = $post['tgl'];
    $this->tglJatuhTempo = $jatuhTempo;
    $this->keterangan = $post['keterangan'];
    $this->total = $post['total'];
    $this->ppn = $post['ppn'];
    $this->grandTotal = $post['grandTotal'];
    $this->note = $post['note'];
    $this->userId = $this->session->userData('id');
    $this->tenantId = $post['tenantId'];

    $this->db->insert('invoice', $this);

    $data = $this->db->order_by('id', 'desc')->get('invoice')->row();
    return $data->id;
  }

  public function update(){
    $post = $this->input->post();
    $jatuhTempo = date('Y-m-d', strtotime($post['tgl'] . '+'.$post['jatuhTempo'].' day'));
    $data = array(
      'nomer' => $post['nomer'],
      'tgl' => $post['tgl'],
      'tglJatuhTempo' => $jatuhTempo,
      'keterangan' => $post['keterangan'],
      'total' => $post['total'],
      'ppn' => $post['ppn'],
      'grandTotal' => $post['grandTotal'],
      'note' => $post['note'],
      'userId' => $this->session->userData('id'),
      'tenantId' => $post['tenantId']
    );

    return $this->db->where('id', $post['id'])->update('invoice', $data);
  }

  public function delete(){
    $id = $this->input->post('id');
    
    return $this->db->where('id', $id)->delete('invoice');
  }

  public function cekNomer($key){
    $data = $this->db->select('id, nomer')->like('nomer', $key)->order_by('id', 'desc')->get('invoice')->row();

    return $data;
  }
    
}
