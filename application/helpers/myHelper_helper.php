<?php

// auth
function cekLogin(){
  $CI =& get_instance();
  
  $value = $CI->session->userdata('loggedIn');
  if($value != true){
    flashData('danger', 'Login terlebih dahulu!');
    redirect('auth');
  }
}

function cekFinance(){
  $CI =& get_instance();
  if($CI->session->userData('level') != 1){
    flashData('warning', 'Anda tidak dapat mengakses halaman tersebut!');
    redirect('dashboard');
  }
}

function flashData($type, $value){
  $CI =& get_instance();
  return $CI->session->set_flashData($type, $value);
}

function level($value){
  $text = '';
  switch ($value) {
    case '1':
      $text = "Finance";
      break;
    
    case '2':
      $text = "Pimpinan";
      break;
    
    default:
      $text = "Tenant";
      break;
  }

  return $text;
}

function status($value){
  $text = '';
  switch ($value) {
    case '1':
      $text = '<span class="badge badge-success">Aktif</span>';
      break;
    
    default:
      $text = '<span class="badge badge-danger">Nonaktif</span>';
      break;
  }

  return $text;
}

function setDate($date){
  return date('d-F-Y', strtotime($date));
}

?>