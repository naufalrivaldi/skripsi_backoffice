<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('TenantModel', 'tenant');
    $this->load->model('LokasiModel', 'lokasi');
    $this->load->model('InvoiceModel', 'invoice');
    $this->load->library('form_validation');
    
    cekLogin();
    cekFinance();
	}

	public function index(){
    $data['title'] = 'Tenant';
    $data['no'] = 1;
    $data['tenant'] = $this->tenant->showAll();
    if($_GET){
      $data['filter'] = (object)[
        'status' => $_GET['status']
      ];
    }else{
      $data['filter'] = (object)[
        'status' => ''
      ];
    }

		$this->load->view('admin/tenant/index', $data);
  }

  public function view($id){
    $data['title'] = 'Tenant';
    $data['tenant'] = $this->tenant->getData($id);
    $data['invoice'] = $this->invoice->showTenantInvoice($id);
    $data['cekPembayaran'] = $this->invoice->cekPembayaran($id);
    $data['no'] = 1;

    $this->load->view('admin/tenant/view', $data);
  }

  public function viewInvoice($id){
    $data['title'] = 'Tenant';
    $data['invoice'] = $this->invoice->getData($id);

    $this->load->view('admin/tenant/view_invoice', $data);
  }
    
  public function form($id = null){
    $data['title'] = 'Tenant';
    $data['lokasi'] = $this->lokasi->showAll();
    
    if(empty($id)){
      $data['tenant'] = (object)[
        'id' => '',
        'kode' => '',
        'nama' => '',
        'pic' => '',
        'telp' => '',
        'status' => '',
        'lokasiId' => '',
      ];
    }else{
      $data['tenant'] = $this->tenant->getData($id);
    }

    $this->load->view('admin/tenant/form', $data);
  }

  public function store(){
    $tenant = $this->tenant;
    $validation = $this->form_validation;
    $validation->set_rules($tenant->rules());
    
    if($validation->run()){
      $tenant->save();
      flashData('success', 'Simpan data berhasil.');
      redirect('tenant');
    }
    
    flashData('danger', 'Simpan data gagal!');
    $this->form();
  }

  public function update(){
    $tenant = $this->tenant;
    $validation = $this->form_validation;
    $validation->set_rules($tenant->rules());

    if($validation->run()){
      $tenant->update();
      flashData('success', 'Update data berhasil.');
      redirect('tenant');
    }

    flashData('danger', 'Simpan data gagal!');
    $this->form($this->input->post('id'));
  }

  public function delete(){
    $this->tenant->delete();
  }

  public function loadLokasi(){
    $json = [];
    $key = '';

    if(!empty($this->input->post('q'))){
      $key = $this->input->post('q');
    }

    $data = $this->db->like('kode', $key)->or_like('nama', $key)->get('lokasi')->result();
    echo json_encode($data);
  }

}
