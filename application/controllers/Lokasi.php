<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('LokasiModel', 'lokasi');
    $this->load->library('form_validation');
    
    cekLogin();
    cekFinance();
	}

	public function index(){
    $data['title'] = 'Lokasi';
    $data['no'] = 1;
    $data['lokasi'] = $this->lokasi->showAll();

		$this->load->view('admin/lokasi/index', $data);
  }
    
  public function form($id = null){
    $data['title'] = 'User';
    
    if(empty($id)){
      $data['lokasi'] = (object)[
        'id' => '',
        'kode' => '',
        'nama' => ''
      ];
    }else{
      $data['lokasi'] = $this->lokasi->getData($id);
    }

    $this->load->view('admin/lokasi/form', $data);
  }

  public function store(){
    $lokasi = $this->lokasi;
    $validation = $this->form_validation;
    $validation->set_rules($lokasi->rules());
    
    if($validation->run()){
      $lokasi->save();
      flashData('success', 'Simpan data berhasil.');
      redirect('lokasi');
    }
    
    flashData('danger', 'Simpan data gagal!');
    $this->form();
  }

  public function update(){
    $lokasi = $this->lokasi;
    $validation = $this->form_validation;
    $validation->set_rules($lokasi->rules());

    if($validation->run()){
      $lokasi->update();
      flashData('success', 'Update data berhasil.');
      redirect('lokasi');
    }

    flashData('danger', 'Simpan data gagal!');
    $this->form($this->input->post('id'));
  }

  public function delete(){
    $this->lokasi->delete();
  }

}
